FROM centos:7

ENV VAULT_VERSION="0.11.1"
RUN yum clean all && \
    yum -y install epel-release && \
    yum -y install wget unzip jq && \
    cd /tmp && \
    wget https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip && \
    unzip -d /bin vault_${VAULT_VERSION}_linux_amd64.zip

COPY entry.sh /entry.sh
RUN chmod 755 /entry.sh
ENTRYPOINT ["/entry.sh"]

